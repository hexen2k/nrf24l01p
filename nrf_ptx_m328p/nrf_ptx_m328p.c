/*
 * nrf_ptx_m328p.c
 *
 * Created: 2014-11-28 16:07:37
 *  Author: michal
 */ 


#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "../lib/uart.h"
#include "../lib/spi.h"
#include "../lib/hal_nrf_reg.h"
#include "../lib/nrf24l01p.h"

#define BAUD 250000
#define RF_PAYLOAD_LENGTH	1
#define RF_CHANNEL 120

uint8_t address_tx[]={0xCE,0xCE,0xF1,0xF3,0xF4};	//LSB byte first, TX_ADDR in PTX, RX_ADDR_0 in PRX
//uint8_t address_rx[]={0x64,0x64,0xF2};	//LSB byte first, TX_ADDR in PRX, RX_ADDR_0 in PTX
uint8_t databuffer[32]={'H'};	//max size one payload in nrf24l01p

void Init(void);
void nrf_Init(void);

int main(void) {	
	Init();
	
	sei();			
    while(1) {	
		_delay_ms(50);

		nrf_write_multibyte_reg(HAL_NRF_TX_PLOAD,databuffer,1,0);	//send only 1 byte for test
		//trigger TX
		PORTD |= 1<<PD3;	// CE=high, trigger TX mode
		_delay_us(15);	//wait >10us
		PORTD &= ~(1<<PD3);	// CE=low, end trgiger TX mode

    }
}


void Init(void){	
	//TODO: pull-up unused pins
		
	uart_init((UART_BAUD_SELECT((BAUD),F_CPU)));
	SPI_MasterInit();
	nrf_Init();
	//INT0
	EICRA |= 1<<ISC01;	//falling edge INT0
	EIMSK |= 1<<INT0;	//enable INT0 interrupt
	
	//blink led
	DDRC |= (1<<PC4) | (1<<PC5);	//out
	PORTC &= ~(1<<PC4);	//low
}

void nrf_Init(void){
	_delay_ms(100);	//Power on reset=100ms (little more, depends on circuit and voltage regulator)
	
	DDRD |= 1<<DDD3;	//CE is output
	PORTD |= 1<<PD2;	//pull-up IRQ
	
// generally OK, but sometimes is problem with correct power up; FIX: pull up CSN(SS) pin externally
/*
	nrf_write_register(SETUP_AW, 0x01);	//address field width = 3 bytes (see size address_Xx tables)
	nrf_write_register(SETUP_RETR, ARD_1500US | ARC_3);	//auto retransmit delay=1500us (250kbps), auto retransmit count=3
	nrf_write_register(RF_CH, RF_CHANNEL);	// frequency channel=2400 + RF_CHANNEL MHz
	nrf_write_register(RF_SETUP, 1<<RF_DR_LOW | HAL_NRF_0DBM<<1);	//data rate=250kbps, power=0dBm
	nrf_write_multibyte_reg(HAL_NRF_PIPE0,address_tx,sizeof address_tx,0);	//write RX_ADDR_P0 in PTX device
	nrf_write_multibyte_reg(HAL_NRF_TX, address_tx, sizeof address_tx,0);	//write TX_ADDR in PTX device
	nrf_write_register(RX_PW_P0, 32);	//32 bytes RX payload - probably not needed in dynamic payload length ////////////////////////////////////// TODO: test comment this line
	nrf_write_register(FEATURE, 1<<EN_DPL | 1<<EN_ACK_PAY);	//enable Dynamic Payload Length and Payload with ACK feature
	nrf_write_register(DYNPD, 1<<DPL_P0);	//Enable dynamic payload length data pipe 0 (Requires  EN_DPL  and  ENAA_P0 )	
	
	nrf_write_register(CONFIG, 1<<EN_CRC | 1<<PWR_UP);	//PTX mode, power UP
	_delay_ms(5);	//wait Tpd2stby
*/

// fully OK, without pull up resistor
	nrf_write_register(STATUS, 0xFF);	//clear interrupt flags
	nrf_write_register(FLUSH_TX,0);	//remove any data
	nrf_write_register(FLUSH_RX,0);	//remove any data
	
	nrf_write_register(EN_RXADDR, 0);	//disable all pipes
	nrf_write_register(EN_AA, 0);	//disable all ACK pipes
	nrf_write_register(EN_RXADDR, 1<<HAL_NRF_PIPE0);	//enable data pipe 0
	nrf_write_register(EN_AA, 1<<HAL_NRF_PIPE0);		//enable ACK data pipe 0
	nrf_write_register(CONFIG, (nrf_read_register(CONFIG) & ~(1<<CRCO)) | 1<<EN_CRC);	//enable 1 byte CRC
	//nrf_write_register(CONFIG, nrf_read_register(CONFIG) | 1<<CRCO | 1<<EN_CRC);	//enable 2 byte CRC
	
	nrf_write_register(SETUP_RETR, ARD_1500US | ARC_3);	//auto retransmit delay=1500us (250kbps), auto retransmit count=3
	nrf_write_register(SETUP_AW, 0x01);	//address field width = 3 bytes (see size address_Xx tables)
	nrf_write_multibyte_reg(HAL_NRF_TX, address_tx, 0, 0);	//set device address
	nrf_write_multibyte_reg(HAL_NRF_PIPE0, address_tx, 0, 0);	//set receiving address on pipe0
	nrf_write_register(FEATURE, nrf_read_register(FEATURE) | 1<<EN_ACK_PAY);	//try enable payload with ACK
	// When the features are locked, the FEATURE and DYNPD are read out 0x00
	// even after we have tried to enable ack payload. This mean that we need to
	// activate the features.
	if(nrf_read_register(FEATURE) == 0x00 && (nrf_read_register(DYNPD) == 0x00)){	//TODO: looks little weird
		nrf_lock_unlock();
		nrf_write_register(FEATURE, nrf_read_register(FEATURE) | 1<<EN_ACK_PAY);	//enable payload with ACK
	}
	nrf_write_register(FEATURE, nrf_read_register(FEATURE) | 1<<EN_DPL);	//enable dynamic payload length
	nrf_write_register(DYNPD, 0x3F);	//enable dynamic payload length for all data pipes
	
	//depends from device mode PRX or PTX
	//nrf_write_register(CONFIG, nrf_read_register(CONFIG) | 1<<PRIM_RX);	//for PRX device PRIM_RX=1
	nrf_write_register(CONFIG, nrf_read_register(CONFIG) & ~(1<<PRIM_RX));	//for PTX device PRIM_RX=0
	//nrf_write_register(RX_PW_P0, RF_PAYLOAD_LENGTH);	//byte payload length ---- only for PRX device
	
	nrf_write_register(RF_CH, RF_CHANNEL);
	nrf_write_register(RF_SETUP, 1<<RF_DR_LOW | HAL_NRF_0DBM<<1);	//data rate=250kbps, power=0dBm
	nrf_write_register(CONFIG, nrf_read_register(CONFIG) | 1<<PWR_UP);	//power up device
	_delay_ms(5);	//wait Tpd2stby

	
}

void SPI_MasterInit(void){
	/* Set MOSI,SCK and SS-CSN output, all others input */
	DDRB |= (1<<PB3)|(1<<PB5)|(1<<PB0)|(1<<PB2);
	PORTB |= 1<<PB1 | 1<<PB4;	//pull up SS, MISO
	/* Enable SPI, Master, set clock rate fck/16 */
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);
}

char SPI_MasterTransmit(char cData){
	/* Start transmission */
	SPDR = cData;
	/* Wait for transmission complete */
	while(!(SPSR & (1<<SPIF)))
		;	
	return SPDR;
}

ISR(INT0_vect){	//triggered by IRQ pin from nrf24l01p
	
	PORTC ^= 1<<PC5;
	
	switch ((nrf_write_register(STATUS, 1<<MAX_RT | 1<<TX_DS | 1<<RX_DR)) & (1<<MAX_RT | 1<<TX_DS | 1<<RX_DR))	//read status register and clear interrupt flags
	{
		case (1<<HAL_NRF_MAX_RT):					// Max retries reached
			nrf_write_register(FLUSH_TX, 0);	// flush tx fifo, avoid fifo jam
			//uart_putc(0x01);
		break;
			
		case (1<<HAL_NRF_TX_DS):                  // Packet sent, and ACK received
			//nrf_write_register(FLUSH_TX,0);		//ignore temporary ACK payload
			//uart_putc(0x02);
		break;
			
		case (1<<HAL_NRF_RX_DR):                  // Packet received
		while (! ((nrf_write_register(NOP,0) & 0x0E) == 0x0E))	//while RF FIFO is not Empty RX_P_NO != 111
			nrf_read_multibyte_reg(HAL_NRF_RX_PLOAD, databuffer);	//read payload data
			//uart_putc(0x03);
		break;
			
		case ((1<<HAL_NRF_RX_DR)|(1<<HAL_NRF_TX_DS)): // Ack payload recieved
		while (! ((nrf_write_register(NOP,0) & 0x0E) == 0x0E))	//while RF FIFO is not Empty RX_P_NO != 111
			nrf_read_multibyte_reg(HAL_NRF_RX_PLOAD, databuffer);	//read payload ACK data
			//uart_putc(0x04);
		break;
			
		default:
			//uart_putc(0x05);
		break;
	}
	
}
