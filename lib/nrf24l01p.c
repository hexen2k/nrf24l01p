#include "spi.h"
#include "hal_nrf_reg.h"
#include "nrf24l01p.h"

/* Reading single register 
or Read RX Payload width
*/
uint8_t nrf_read_register(uint8_t reg){
	uint8_t tmp;
	CSN_LOW();
	SPI_MasterTransmit(reg);
	tmp=SPI_MasterTransmit(0);
	CSN_HIGH();
	return tmp;
}

/* Writing single byte to register, 
if used command:
NOP,
FLUSH_RX,
FLUSH_TX,
REUSE_TX_PL,
nothing is written
*/
uint8_t nrf_write_register(uint8_t reg, uint8_t val){
	uint8_t status_reg;
	CSN_LOW();
	status_reg=SPI_MasterTransmit(WRITE_REG | reg);
	if(reg!=NOP && reg!=FLUSH_TX && reg!=FLUSH_RX && reg!=REUSE_TX_PL) SPI_MasterTransmit(val);
	CSN_HIGH();
	return status_reg;
}

/* read multi-byte registers:
HAL_NRF_PIPE0,
HAL_NRF_PIPE1,
HAL_NRF_TX,
HAL_NRF_RX_PLOAD,

TODO: return value
 @return pipe# of received data (MSB), if operation used by a hal_nrf_read_rx_pload
 @return length of read data (LSB), either for hal_nrf_read_rx_pload or
  for hal_nrf_get_address.
 
*/
uint16_t nrf_read_multibyte_reg(uint8_t reg, uint8_t *pbuf){
	uint8_t ctr, length;
	switch(reg){
		case HAL_NRF_PIPE0:
		case HAL_NRF_PIPE1:
		case HAL_NRF_TX:
		length = ctr = (nrf_read_register(SETUP_AW)+2); //RX/TX Address field width 
		CSN_LOW();
		SPI_MasterTransmit(RX_ADDR_P0 + reg);
		break;
		
		case HAL_NRF_RX_PLOAD:
		if( (reg = ((nrf_write_register(NOP,0) & 0x0E)>>1) ) < 7){	//read Data pipe number for the payload available for reading from  RX_FIFO		
			length = ctr = nrf_read_register(RD_RX_PLOAD_W);
			CSN_LOW();
			SPI_MasterTransmit(RD_RX_PLOAD);
		} else ctr = length = 0;		
		break;

		default:
		ctr = length = 0;
		break;
	}

	while(ctr--) *pbuf++ = SPI_MasterTransmit(0);
	
	CSN_HIGH();
  
	return (((uint16_t) reg << 8) | length);
}

/* write multi-byte registers:
HAL_NRF_PIPE0,
HAL_NRF_PIPE1,
HAL_NRF_TX,
then need only reg and *pbuf argument
HAL_NRF_TX_PLOAD,
HAL_NRF_TX_PLOAD_NOACK,
then need reg, *pbuf, length argument
HAL_NRF_ACK_PLOAD
need all arguments
*/
void nrf_write_multibyte_reg(uint8_t reg, uint8_t *pbuf, uint8_t length, uint8_t pipe){
	switch(reg){
		case HAL_NRF_PIPE0:
		case HAL_NRF_PIPE1:
		case HAL_NRF_TX:
		length = (nrf_read_register(SETUP_AW)+2); //RX/TX Address field width 
		CSN_LOW();
		SPI_MasterTransmit(WRITE_REG + RX_ADDR_P0 + reg);
		break;
		
		case HAL_NRF_TX_PLOAD:
		CSN_LOW();
		SPI_MasterTransmit(WR_TX_PLOAD);
		break;
		
		case HAL_NRF_ACK_PLOAD:
		CSN_LOW();
		SPI_MasterTransmit(WR_ACK_PLOAD | pipe);
		break;

		case HAL_NRF_TX_PLOAD_NOACK:
		CSN_LOW();
		SPI_MasterTransmit(WR_NAC_TX_PLOAD);
		break;
				
		default:
		break;
	}

	while(length--) SPI_MasterTransmit(*pbuf++);	

	CSN_HIGH();
}

void nrf_lock_unlock(void){
	CSN_LOW();
	SPI_MasterTransmit(LOCK_UNLOCK);
	SPI_MasterTransmit(0x73);
	CSN_HIGH();
}