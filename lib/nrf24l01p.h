#ifndef NRF24L01P_H_
#define NRF24L01P_H_

#include <avr/io.h>

#define CSN_LOW()	PORTB&=~(1<<PB0)
#define CSN_HIGH()	PORTB|=(1<<PB0)

uint8_t nrf_read_register(uint8_t reg);
uint8_t nrf_write_register(uint8_t reg, uint8_t val);
uint16_t nrf_read_multibyte_reg(uint8_t reg, uint8_t *pbuf);
void nrf_write_multibyte_reg(uint8_t reg, uint8_t *pbuf, uint8_t length, uint8_t pipe);
void nrf_lock_unlock(void);

#endif //NRF24L01P_H_